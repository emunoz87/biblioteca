import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'books',
    loadChildren: () => import('./books/books.module').then( m => m.BooksPageModule)
  },
  {
    path: '',
    redirectTo: 'books',
    pathMatch: 'full'
  },
  {
    path: "books",
    children: [
      {
        path: "",
        loadChildren: "./books/books.module#BooksPageModule",
      },
      {
        path: "add",
        loadChildren: "./books/add/add.module#AddPageModule",
      },
      {
        path: "view/:bookId",
        loadChildren: "./books/view/view.module#ViewPageModule",
      },
      {
        path: "edit/:bookId",
        loadChildren: "./books/edit/edit.module#EditPageModule",
      }
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
