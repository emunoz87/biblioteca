import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from "@angular/router";
import { BooksService } from "../books.service";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { books } from '../books.model';

@Component({
  selector: "app-edit",
  templateUrl: "./edit.page.html",
  styleUrls: ["./edit.page.scss"],
})
export class EditPage implements OnInit {
  book: books;
  formBookEdit: FormGroup;

  constructor(
    private activeRouter: ActivatedRoute,
    private serviceBook: BooksService,
    private router: Router
  ) {}

  ngOnInit() {

    this.activeRouter.paramMap.subscribe(paramMap => {
      if (!paramMap.has('bookId')) {
        return;
      }
      const bookId = parseInt(paramMap.get('bookId'));
      this.book = this.serviceBook.getBook(bookId);

    });

    this.formBookEdit = new FormGroup({
      pquantity: new FormControl(this.book.quantity, {
        updateOn: "blur",
        validators: [Validators.required, Validators.min(1)],
      }),
      pcode: new FormControl(this.book.code, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(1)],
      }),
      pauthor: new FormControl(this.book.author, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pdescription: new FormControl(this.book.description, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(20)],
      }),
      pgenre: new FormControl(this.book.genre, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      ppublisher: new FormControl(this.book.publisher, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pprice: new FormControl(this.book.price, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      ptitle: new FormControl(this.book.title, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
    });

    this.formBookEdit.value.ptitle = this.book.title;

  }

  editBook() {

    if (!this.formBookEdit.valid) {
      return;
    }

    this.serviceBook.editBook(
      this.formBookEdit.value.pcode,
      this.formBookEdit.value.pauthor,
      this.formBookEdit.value.ptitle,
      this.formBookEdit.value.pgenre,
      this.formBookEdit.value.ppublisher,
      this.formBookEdit.value.pquantity,
      this.formBookEdit.value.pprice,
      this.formBookEdit.value.pdescription
      
    );

    this.formBookEdit.reset();
    
    this.router.navigate(['./books']);
    
  }
}

