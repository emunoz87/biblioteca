import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { BooksService } from '../books.service';
import { books } from '../books.model';

@Component({
  selector: 'app-view',
  templateUrl: './view.page.html',
  styleUrls: ['./view.page.scss'],
})
export class ViewPage implements OnInit {
  book: books;
  constructor(
    private activeRouter: ActivatedRoute,
    private BooksService: BooksService,
    private router: Router,
    private alertController: AlertController
  ) { }
  
  ngOnInit() {
    
    this.activeRouter.paramMap.subscribe(
      paramMap => {
        if(!paramMap.has('bookId')){
          return;
        }
        const bookId =  parseInt( paramMap.get('bookId'));
        this.book = this.BooksService.getBook(bookId);
        console.log(bookId);
      }
    );
  }

  deleteBook(){
    this.alertController.create({
      header: "Borrar Libro",
      message: "Esta seguro que desea borrar este Libro?",
      buttons:[
        {
          text:"No",
          role: 'no'
        },
        {
          text: 'Borrar',
          handler: () => {
            this.BooksService.deleteBook(this.book.code);
            this.router.navigate(['./books']);
          }
        } 
      ]
    })
    .then(
      alertEl => {
        alertEl.present();
      }
    );
    
  }

  update(code: number) {
    this.router.navigate(["/books/edit/" + code]);
  }
}
