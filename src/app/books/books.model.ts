export interface books{
    code: number;
    author: string;
    title: string;
    genre: string;
    publisher: string;
    quantity: number;
    price: number;
    description: string;
}