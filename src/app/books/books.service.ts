import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { books } from "./books.model";

@Injectable({
  providedIn: "root",
})
export class BooksService {
  private books: books[] = [
    {
      code: 12345,
      author: "Herman Hesse",
      title: "Demian",
      genre: "Filosofía",
      publisher: "Porrua",
      description: "Demian: Die Geschichte von Emil Sinclairs Jugend (en español, Demian: Historia de la juventud de Emil Sinclair) es una novela que relata en primera persona el paso de la niñez a la madurez de este personaje, del escritor alemán Hermann Hesse. La obra fue publicada por primera vez en 1919, en los tiempos que siguieron a la Gran Guerra.",
      price: 15000,
      quantity: 67,
    },
  ];
  constructor(
    private http: HttpClient
  ) {}

  getAll() {
    return [...this.books];
  }

  getBook(bookId: number) {
    return {
      ...this.books.find((book) => {
        return book.code === bookId;
      }),
    };
  }

  deleteBook(bookId: number) {
    this.books = this.books.filter((book) => {
      return book.code !== bookId;
    });
  }

  addBook(
    pcode: number,
    pauthor: string,
    ptitle: string,
    pgenre: string,
    ppublisher: string,
    pquantity: number,
    pprice: number,
    pdescription: string,    
  ) {
    const book: books = {
      code: pcode,
      author: pauthor,
      title: ptitle,
      genre: pgenre,
      publisher: ppublisher,
      quantity: pquantity,
      price: pprice,
      description: pdescription,
    }
    this.http.post('https://biblioteca-64168.firebaseio.com/addBook.json',
       { 
          ...book, 
          id:null
        }).subscribe(() => {
          console.log('entro');
        });

    this.books.push(book);
  }

  editBook(
    pcode: number,
    pauthor: string,
    ptitle: string,
    pgenre: string,
    ppublisher: string,
    pquantity: number,
    pprice: number,
    pdescription: string
    
  ) {
    let index = this.books.map((x) => x.code).indexOf(pcode);

    this.books[index].code = this.books.map((x) => x.code).indexOf(pcode);
    this.books[index].author = pauthor;
    this.books[index].title = ptitle;
    this.books[index].genre = pgenre;
    this.books[index].publisher = ppublisher;
    this.books[index].quantity = pquantity;
    this.books[index].price = pprice;
    this.books[index].description = pdescription;

    console.log(this.books);
  }

}
