import { Component, Input, OnInit } from "@angular/core";
import { BooksService } from "./books.service";
import { books } from "./books.model";
import { Router } from "@angular/router";
import { AlertController } from "@ionic/angular";

@Component({
  selector: "page-books",
  templateUrl: "./books.page.html",
  styleUrls: ["./books.page.scss"],
})
export class BooksPage implements OnInit {
  @Input() author: string;
  book: books[];
  constructor(
    private BooksServices: BooksService,
    private router: Router,
    private alertController: AlertController
  ) {}

  ngOnInit() {
    console.log("Carga inicial");
    this.book = this.BooksServices.getAll();
  }

  ionViewWillEnter() {
    console.log("Se obtuvo la lista");
    this.book = this.BooksServices.getAll();
  }

  view(code: number) {
    this.router.navigate(["/books/view/" + code]);
  }

  delete(code: number) {
    this.alertController
      .create({
        header: "Borrar Libro",
        message: "Esta seguro que desea borrar este libro?",
        buttons: [
          {
            text: "No",
            role: "no",
          },
          {
            text: "Borrar",
            handler: () => {
              this.BooksServices.deleteBook(code);
              this.book = this.BooksServices.getAll();
            },
          },
        ],
      })
      .then((alertEl) => {
        alertEl.present();
      });
  }

  update(code: number) {
    this.router.navigate(["/books/edit/" + code]);
  }

}
