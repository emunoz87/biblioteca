import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { BooksService } from "../books.service";

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {
  formBookAdd: FormGroup;
  constructor(private serviceBooks: BooksService, private router: Router) {}

  ngOnInit() {

    this.formBookAdd = new FormGroup({
      pquantity: new FormControl(1, {
        updateOn: "blur",
        validators: [Validators.required, Validators.min(1)],
      }),
      pcode: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(1)],
      }),
      pauthor: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pdescription: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(20)],
      }),
      pgenre: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      ppublisher: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pprice: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      ptitle: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
    });
  }

  addBook() {

    if (!this.formBookAdd.valid) {
      return;
    }

    this.serviceBooks.addBook(
      this.formBookAdd.value.pcode,
      this.formBookAdd.value.pauthor,
      this.formBookAdd.value.ptitle,
      this.formBookAdd.value.pgenre,
      this.formBookAdd.value.ppublisher,
      this.formBookAdd.value.pquantity,
      this.formBookAdd.value.pprice,
      this.formBookAdd.value.pdescription

    );

    this.formBookAdd.reset();
    
    this.router.navigate(["/books"]);
    
  }
}
